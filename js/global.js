/**
 *
 * Copyright (c) 2010 Pedro Fuentes ( http://pedrofuent.es )
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 **/

var m;
var map;
var geocoder;
var marker;
var comuna	= "";
var region	= "";
var offline	= false;

	$(document).ready(function(){
		
		$('#school').focus();
		
		if(!offline){
			var latlng = new google.maps.LatLng(-33.4253598, -70.5664659);
			var myOptions = {
				zoom: 8,
				center: latlng,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			m = new google.maps.Map(document.getElementById("map"), myOptions);
		
			geocoder = new google.maps.Geocoder();
		}
		
		if($('#region_id').size() == 1){
			region	= $('#region_id').val();
			$('#note-comuna').html("* You are looking Comunas in <strong>" + $('#region').val() + "</strong>");
			$('#note-school').html("* You are looking Schools in <strong>" + $('#region').val() + " and all Comunas inside</strong>");
		}
		
		$('#comuna').autocomplete('json/show-comunas.php', {
			autoFill: false,
			minChars: 2,
			width: 271,
			dataType: 'json',
			matchCase: true,
			mustMatch: true,
			cacheLength: 1,
			extraParams: {
				r: function() { return region; }
			},
			parse: function(data){
				return $.map(data, function(row) {
					return {
						data: row,
						value: row.Name,
						result: row.Name
					}
				});
			},
			formatItem: function(item) {
				return item.Name;
			}
		}).result(function(e,d,f){
			
			$('#note-school').html("* You are looking Schools in Comuna <strong>" + d.Name + "</strong>");
			
			$('#school').val('');
			comuna	= d.Id;
			$('#school').flushCache();
			$('#school').focus();
			$('#clear-comuna').show().click(function(){
				$('#comuna').val('').flushCache();
				$('#school').val('').flushCache();
				$(this).hide();
				$('#clear-school').hide();
				comuna	= "";
				
				if(region == ""){
					$('#note-school').html('* Your are looking Schools on <strong>all</strong> Regiones and Comunas');
				} else {
					$('#note-school').html("* You are looking Schools in <strong>" + $('#region').val() + " and all Comunas inside</strong>");
				}
				
			});
		
		});
		
		$('#school').autocomplete('json/show-schools.php', {
			autoFill: true,
			minChars: 2,
			width: 271,
			dataType: 'json',
			matchCase: true,
			mustMatch: true,
			cacheLength: 1,
			extraParams: {
				c: function() { return comuna; },
				r: function() { return region; }
			}
			,
			parse: function(data){
				return $.map(data, function(row) {
					return {
						data: row,
						value: row.Name,
						result: row.Name
					}
				});
			},
			formatItem: function(item) {
				return item.Name;
			}
		}).result(function(e,d,f){
			
			$('#current-school').val(d.Id);
			
			$.ajax({
				type: "GET",
				url: "json/show-school.php?id=" + d.Id,
				dataType: 'json',
				success: function(data){
					
					$("table#school-data td.coordinates").html('');
					
					if(data.lat == null){
						
						if(data.utm_x == null){
							
							setLocation(data.direccion+", "+data.comuna+", Chile");
							
						} else {
							
							var latlon=Array(2);
							UTMXYToLatLon(data.utm_x,data.utm_y,19,true,latlon);
							
							$("table#school-data td.coordinates").html(RadToDeg (latlon[0]) + ", " + RadToDeg (latlon[1]));
							
							setMarker(RadToDeg (latlon[0]),RadToDeg (latlon[1]));
							
						}
						
					} else {
						
						$("table#school-data td.coordinates").html(data.lat + ", " + data.lng);
						
						setMarker(data.lat,data.lng);
						
					}
					
					$.each(data, function(i,v){
						
						$("table#school-data td."+i).html(v);
						
					});
					
				}
			});
			
			$('#clear-school').show().click(function(){
				$('#school').val('').flushCache();
				$(this).hide();
				$('#school').focus();
			});
			
		});
		
	});
	
	function setLocation(a){
		
		if(!offline){
			
			$("table#school-data td.coordinates").html('');
		
			if(m.getZoom() != 14){
				m.setZoom(14);
			}
		
			if(marker){
				marker.setMap(null);
			}
		
			geocoder.geocode( { 'address': a }, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					$("table#school-data td.coordinates").html(results[0].geometry.location.b + ", " + results[0].geometry.location.c);
					m.setCenter(results[0].geometry.location);
					marker = new google.maps.Marker({
						map: m, 
						position: results[0].geometry.location
					});
				} else {
				
					switch(status){
						case 'ZERO_RESULTS':
							$("table#school-data td.coordinates").html('<span class="red">Google didn\'t find the coordinates</a>');
							break;
						default:
							alert("Geocode was not successful for the following reason: " + status);
							break;
					}
				
				}
			});
		
		}
	}
	
	function setMarker(lat,lng){
		
		var latlng	= new google.maps.LatLng(lat,lng);
		
		if(m.getZoom() != 14){
			m.setZoom(14);
		}
		
		if(marker){
			marker.setMap(null);
		}
		
		m.setCenter(latlng);
		
		marker = new google.maps.Marker({
			map: m, 
			position: latlng
		});
		
	}
