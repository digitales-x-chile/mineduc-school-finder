<?php
/**
 *
 * Copyright (c) 2010 Pedro Fuentes ( http://pedrofuent.es )
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 **/
?>
<?php require_once( dirname(__FILE__) . "/../inc/global.php" ); ?>
<?php
	
	$q1 = $dbc->query("SELECT 
	  comunas.name,
	  comunas.id
	FROM
	  provincias
	  INNER JOIN comunas ON (provincias.id = comunas.provincia)
	  INNER JOIN schools ON (comunas.id = schools.comuna)
	WHERE
	  comunas.name LIKE '%".$dbc->safeSQL(urldecode($_GET['q']))."%'"
	. ( ( trim($_GET['r']) != "" ) ? " AND provincias.region = '" . $dbc->safeSQL($_GET['r']) . "'" : "" ) . "
	GROUP BY comunas.id");
	
	while($r1	= $dbc->fetch($q1)){
		
		$d[]	= array("Id"=>$r1['id'],"Name"=>$r1['name']);
		
	}
	
	echo json_encode($d);
	
?>