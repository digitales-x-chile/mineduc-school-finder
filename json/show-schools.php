<?php
/**
 *
 * Copyright (c) 2010 Pedro Fuentes ( http://pedrofuent.es )
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 **/
?>
<?php require_once( dirname(__FILE__) . "/../inc/global.php" ); ?>
<?php
	
	$q1 = $dbc->query("SELECT 
	  schools.rbd,
	  schools.establecimiento
	FROM
	  regiones
	  INNER JOIN provincias ON (regiones.id = provincias.region)
	  INNER JOIN comunas ON (provincias.id = comunas.provincia)
	  INNER JOIN schools ON (comunas.id = schools.comuna)
	WHERE
	  schools.establecimiento LIKE '%".$dbc->safeSQL(urldecode($_GET['q']))."%'
	" . (  ( trim($_GET['c']) != "" ) ? " AND schools.comuna = '" . $dbc->safeSQL($_GET['c']) . "'" : "" ) . "
	" . (  ( trim($_GET['r']) != "" ) ? " AND regiones.id = '" . $dbc->safeSQL($_GET['r']) . "'" : "" ) . "
	LIMIT " . $_GET['limit']);
	
	while($r1	= $dbc->fetch($q1)){
		
		$d[]	= 	array(
						"Id"	=> $r1['rbd'],
						"Name"	=> $r1['establecimiento']
					);
		
	}
	
	echo json_encode($d);
	
?>