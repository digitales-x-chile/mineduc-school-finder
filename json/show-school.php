<?php
/**
 *
 * Copyright (c) 2010 Pedro Fuentes ( http://pedrofuent.es )
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 **/
?>
<?php require_once( dirname(__FILE__) . "/../inc/global.php" ); ?>
<?php
	
	$r1	= $dbc->fetch($dbc->query("SELECT 
	  schools.rbd,
	  schools.comuna,
	  schools.departamento_provincial,
	  schools.establecimiento,
	  schools.direccion,
	  schools.ubicacion,
	  schools.matricula,
	  schools.ingreso,
	  schools.utm_x,
	  schools.utm_y,
	  schools.lat,
	  schools.lng,
	  regiones.name AS region_name,
	  comunas.name AS comuna_name,
	  regiones.id AS region_id
	FROM
	  regiones
	  INNER JOIN provincias ON (regiones.id = provincias.region)
	  INNER JOIN comunas ON (provincias.id = comunas.provincia)
	  INNER JOIN schools ON (comunas.id = schools.comuna)
	WHERE
	  schools.rbd = '" . $dbc->safeSQL($_GET['id']) . "'
	LIMIT 1"));
	
	$data	=	array(
					"rbd"				=> $r1['rbd'],
					"comuna"			=> $r1['comuna_name'],
					"establecimiento"	=> $r1['establecimiento'],
					"direccion"			=> $r1['direccion'],
					"ubicacion"			=> $r1['ubicacion'],
					"matricula"			=> $r1['matricula'],
					"ingreso"			=> $r1['ingreso'],
					"utm_x"				=> ( ( $r1['utm_x'] == 0 ) ? null : $r1['utm_x'] ),
					"utm_y"				=> ( ( $r1['utm_y'] == 0 ) ? null : $r1['utm_y'] ),
					"lat"				=> ( ( $r1['lat'] == 0 ) ? null : $r1['lat'] ),
					"lng"				=> ( ( $r1['lng'] == 0 ) ? null : $r1['lng'] )
				);
	
	echo json_encode($data);
	
?>