<?php
/**
 * Clase para conexión a MySQL
 * 
 * Clase para conexión a base de datos MySQL
 * @author OXUS LTDA. <info@oxus.cl>
 * @version 1.0
 * @copyright Copyright (c) 2004-2010, OXUS LTDA.
 * @package mysql
 */
 
 class Database_Mysql {
	
	/**
	 * Host o IP del servidor MySQL
	 * @access private
	 * @var string
	*/
	var $databaseHost		= DB_HOST;
	
	/**
	 * Usuario para acceder a la base de datos
	 * @access private
	 * @var string
	*/
	var $databaseUser		= DB_USERNAME;
	
	/**
	 * Clave para acceder a la base de datos
	 * @access private
	 * @var string
	*/
	var $databasePassword	= DB_PASSWORD;
	
	/**
	 * Base de datos
	 * @access private
	 * @var string
	*/
	var $databaseName		= DB_DATABASE;
	
	/**
	 * Recurso de la consulta ejecutada
	 * @access private
	 * @var resource
	*/
	var $databaseQuery		= ""; // la consulta
	
	/**
	 * Contador de consultas
	 * @access private
	 * @var int
	*/
	var $queryCount			= 0;
	
	/**
	 * Almacena el error
	 * @access private
	 * @var string
	*/
	var $error				= "";
	
	/**
	 * Almacena el link a la conexión a la base de datos
	 * @access private
	 * @var resource
	*/
	var $_databaseLink		= "";
	
	/**
	 * Almacena la consulta
	 * @access private
	 * @var string
	*/
	var $query;
	
	/**
	 * Constructor que crea la conexión a la base de datos
	*/
	function Database_Mysql() {
		$this->_databaseLink	= mysql_connect($this->databaseHost, $this->databaseUser, $this->databasePassword) or $this->error = mysql_error();
		if (!$this->error) mysql_select_db($this->databaseName, $this->_databaseLink) or $this->error = mysql_error();
	}
	
	/**
	 * Ejecuta la consulta
	 * @param string $query
	 * @return resource
	*/
	function query($query) {
		$this->query	= $query;
		$this->queryCount++; // incrementamos el contador de consultas
		$this->databaseQuery	= mysql_query($query, $this->_databaseLink) or $this->error = mysql_error();
		if ($this->error) return false;
		return $this->databaseQuery;
	}
	
	/**
	 * Entrega recurso que puede ser recorrido y que va entregando las filas y sus campos
	 * @param resource $query OPTIONAL
	 * @return resource
	*/
	function fetch($query = false) {
		return mysql_fetch_array(($query ? $query : $this->databaseQuery), MYSQL_BOTH);
	}
	
	/**
	 * Entrega la cantidad de filas obtenidas en un SELECT o SHOW
	 * @param string $query OPTIONAL
	 * @return int
	*/
	function numRows($query = false) {
		return mysql_num_rows(($query ? $query : $this->databaseQuery));
	}
	
	/**
	 * Libera los recursos asociados al identificador
	 * @param resource $query
	 * @return bool
	*/
	function freeResult($query = false) {
		return mysql_free_result(($query ? $query : $this->databaseQuery));
	}
	
	/**
	 * Entrega el id generado en el último insert de una columna AUTO_INCREMENT
	 * @return int
	*/
	function insertId() {
		return mysql_insert_id($this->_databaseLink);
	}
	
	/**
	 * Entrega la cantidad de filas afectadas en un INSERT, UPDATE, REPLACE o DELETE
	 * @return int
	*/
	function affectedRows() {
		return mysql_affected_rows($this->_databaseLink);
	}
	
	/**
	 * Cierra la conexión con la base de datos
	 * @return bool
	*/
	function close() {
		if (!$this->_databaseLink) return false;
		return mysql_close($this->_databaseLink);
	}
	
	/**
	 * Entrega el valor en una consulta en un formato que previene SQL INJECTION
	 * @param string $value
	 * @return string
	*/
	function safeSQL($value){
		return addslashes(htmlspecialchars($value));
	}
	
	/**
	 * Muestra la última consulta ejecutada
	 * @return string
	*/
	function showQuery(){
		return $this->query;
	}
	
	/**
	 * Muestra el último error obtenido
	 * @return string
	*/
	function error(){
		return $this->error;
	}
 }
?>
