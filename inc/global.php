<?php
/**
 * Funciones Generales
 * 
 * Funciones de uso general
 * @author OXUS LTDA. <info@oxus.cl>
 * @version 1.0
 * @copyright Copyright (c) 2004-2010, OXUS LTDA.
 * @package general
 */
	
	/**
	 * Incluimos el archivo d configuración
	 */
	require_once dirname(__FILE__) . "/config.inc.php";
	
	/**
	 * Incluimos la(s) clase para la conexión a MySQL
	 */
	require_once dirname(__FILE__) . "/class.Database_Mysql.php";
	
	session_start();
	$dbc	= new Database_Mysql();
	
	/**
	 * Completa con la cantidad de ceros a la izquierda a $num hasta llegar a $length de largo
	 * @param int $num número
	 * @param int $length largo del número
	 * @return int
	*/
	function inc_length($num,$length){
	
		if ( strlen($num) >= $length ){
			return $num;
		} else {
			return inc_length (0 . $num , $length);
		}
	
	}
	
	/**
	 * Construye para una query de tipo SELECT
	 * @param string $current consulta ya generada a la cual agregar
	 * @param string $add condición a agregar
	 * @return string
	*/
	function queryBuilder($current,$add){
		if(trim($current) == ""){ $query = $add; } else { $query = "( " . $current . " AND " . $add . " ) "; }
		return $query;
	}
	
	/**
	 * Construye para una query de tipo UPDATE
	 * @param string $current consulta ya generada a la cual agregar
	 * @param string $field campo
	 * @param string $value valor
	 * @return string
	*/
	function setBuilder($current,$field,$value){
		if(trim($current) == ""){ $query = " " . $field . " = " . $value; } else { $query = $current . ", " . $field . " = " . $value . ""; }
		return $query;
	}
	
	/**
	 * Limpia un valor retornado por una consulta MySQL cuando este fué guardado con {@link safeMYSQL()}
	 * @param string $value
	 * @return string
	*/
	function safeHTML($value){
		return stripslashes($value);
	}
	
	/**
	 * Entrega el valor en una consulta en un formato que previene SQL INJECTION
	 * @param string $value
	 * @return string
	*/
	function safeMYSQL($value){
		return addslashes(htmlspecialchars($value));
	}
	
	function ageGroup2Txt($value){
		
		switch($value){
			case 1:
				return "< 15";
				break;
			case 2:
				return "15-24";
				break;
			case 3:
				return "25-34";
				break;
			case 4:
				return "35-44";
				break;
			case 5:
				return "> 44";
				break;
			default:
				break;
		}
		
	}
	
?>