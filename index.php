<?php
/**
 *
 * Copyright (c) 2010 Pedro Fuentes ( http://pedrofuent.es )
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 **/
?>
<?php require_once( dirname(__FILE__) . "/inc/global.php" ); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>
	
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta content="digitalesporchile.org" name="author"/>
	<meta content="" name="description" />
	<meta name="verify-v1" content="PTHmYOANA+LXGtIroByQfUCGWcZMjhNgtHrxNksZfDM=" />

	<title>Mineduc School Finder</title>

	<link rel="stylesheet" href="css/screen.css" type="text/css" media="screen, projection"/>
	<link rel="stylesheet" href="css/print.css" type="text/css" media="print"/>
	<!--[if IE]><link rel="stylesheet" href="css/ie.css" type="text/css" media="screen, projection"/><![endif]-->
	<link rel="stylesheet" href="css/standard.css" type="text/css" media="screen, projection"/>
	<link rel="stylesheet" href="css/style.css" type="text/css" media="screen, projection"/>
	<!--[if IE 6]><link rel="stylesheet" href="css/ie6.css" type="text/css" media="screen, projection"/><![endif]-->
	
	<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery.1.4.2.js"></script>
	<script type="text/javascript" src="js/jquery.autocomplete.js"></script>
	<script type="text/javascript" src="js/coordinate_converter.js"></script>
	<script type="text/javascript" src="js/global.js?s=1"></script>	
	
</head>
<body>
	
<div class="container">
	
	<div class="span-24">
		
		<h1>
			School Finder
		</h1>
		
		<h4>
			You can search directly a School or filter by Región and Comuna
		</h4>
		
		<form action="#" method="post">
			
			<div class="span-12">
				<fieldset>
				
						<label>Región</label>
						
							<?php
							
								$q1 = $dbc->query("SELECT 
								  regiones.id,
								  regiones.name
								FROM
								  regiones
								  INNER JOIN provincias ON (regiones.id = provincias.region)
								  INNER JOIN comunas ON (provincias.id = comunas.provincia)
								  INNER JOIN schools ON (comunas.id = schools.comuna)
								GROUP BY regiones.id");
								
								if($dbc->numRows($q1) > 1){
									
									echo '<select id="region">';
									echo '<option selected="">All Where Information is Avaliable</option>';
									
									while($r1	= $dbc->fetch($q1)){

										echo '<option value="' . $r1['id'] . '">'.safeHTML($r1['id']).' - ' . safeHTML($r1['name']) . '</option>';

									}
									
									echo '</select>';
									
								} else {
									
									$r1	= $dbc->fetch($q1);
									echo '
										<input id="region" type="text" readonly="" value="'.safeHTML($r1['id']).' - '.safeHTML($r1['name']).'" />
										<input id="region_id" type="hidden" value="'.$r1['id'].'" />
									';
									
								}
								
							?>
						</select>
					
				</fieldset>
			</div>
			
			<div class="span-12 last">
				<fieldset>
				
						<label>Comuna</label>
					
						<input id="comuna" type="text" value="" />
						
						<img src="images/cancel.png" id="clear-comuna" alt="Clear Comuna" />
						
						<div class="note" id="note-comuna">* Your are looking Comunas on <strong>all</strong> Regiones</div>
					
				</fieldset>
			</div>
			
			<div class="span-24 last">
				<fieldset>
				
						<label>School</label>
					
						<input id="school" type="text" class="long" value="" />
						
						<img src="images/cancel.png" id="clear-school" alt="Clear School" />
						
						<div class="note" id="note-school">* Your are looking Schools on <strong>all</strong> Regiones and Comunas</div>
						
				</fieldset>
			</div>
		
		</form>
	</div>
	
	<div id="result" class="span-24 last">
		
		<table id="school-data">
			<tbody>
				<tr>
					<th>
						School Name
					</th>
					<td colspan="3" class="establecimiento">
						&nbsp;
					</td>
				</tr>
				<tr>
					<th>
						RBD
					</th>
					<td class="rbd">
						&nbsp;
					</td>
					<th>
						Coordinates
					</th>
					<td class="coordinates">
						&nbsp;
					</td>
				</tr>
				<tr>
					<th>
						Address
					</th>
					<td colspan="3" class="direccion">
						&nbsp;
					</td>
				</tr>
				<tr>
					<th>
						Comuna
					</th>
					<td class="comuna">
						&nbsp;
					</td>
					<th>
						Location Type
					</th>
					<td class="ubicacion">
						&nbsp;
					</td>
				</tr>
				<tr>
					<th>
						Students Enrolled
					</th>
					<td class="matricula">
						&nbsp;
					</td>
					<th>
						Tentative Class Start Date
					</th>
					<td class="ingreso">
						&nbsp;
					</td>
				</tr>
			</tbody>
		</table>
		
	</div>
	
	<div class="span-24">
		<div id="map"></div>
	</div>
	
	<div class="span-24 last">
		<br />
		(<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/2.0/cl/">CC</a>) <a href="http://dxc.cl">Digitales por Chile</a>. With the Support of <a href="http://ushahidi.com">Ushahidi</a> and <a href="http://atlas.mineduc.cl">PMG Territorial Mineduc</a>.
	</div>
	
</div>

</body>
</html>